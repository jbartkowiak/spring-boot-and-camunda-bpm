package de.bartkowiak.demo.services.internal.hello;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class HelloDelegate implements JavaDelegate {

    @Autowired
    HelloService sayHelloService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        sayHelloService.echoConsole(delegateExecution.getVariable("userFirstName").toString());
    }
}
