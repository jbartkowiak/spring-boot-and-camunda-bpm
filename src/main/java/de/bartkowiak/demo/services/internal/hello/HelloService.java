package de.bartkowiak.demo.services.internal.hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class HelloService {

    public static Logger logger = LoggerFactory.getLogger(HelloService.class);

    public void echoConsole(String firstname) {
        logger.info(String.format("Hello %1$s!", firstname));
    }
}
