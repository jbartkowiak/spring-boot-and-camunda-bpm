package de.bartkowiak.demo.services.external.name;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NameDelegate implements JavaDelegate {

    @Autowired
    NameService nameService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        delegateExecution.setVariable("userFirstName", nameService.getRandomUserName().getFirst());
    }
}
