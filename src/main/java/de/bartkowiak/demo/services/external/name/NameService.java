package de.bartkowiak.demo.services.external.name;

import de.bartkowiak.demo.domain.user.Name;
import de.bartkowiak.demo.domain.user.RandomUserRequest;
import de.bartkowiak.demo.domain.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class NameService {

    private static final String URL = "https://randomuser.me/api/";
    private static Logger logger = LoggerFactory.getLogger(NameService.class);

    public Name getRandomUserName() {
        logger.info("Requesting random user name.");

        Name name = getRandomUsers().get(0).getName();

        logger.info(String.format("Received random user name with following details: %1$s", name.toString()));

        return name;
    }

    public List<User> getRandomUsers() {
        try {
            logger.info(String.format("Going to request random user from service with endpoint: %1$s", URL));

            RestTemplate restTemplate = new RestTemplate();
            RandomUserRequest randomUserRequest = restTemplate.getForObject(URL, RandomUserRequest.class);

            logger.info("Got random users from service.");

            return randomUserRequest.getUsers();
        } catch (Exception e) {
            logger.error(String.format("Caught exception while requesting user. Will return null. Message was: %1$s", e.getMessage()));

            return null;
        }
    }
}
