package de.bartkowiak.demo.domain.user;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "gender",
        "name",
        "location",
        "email",
        "login",
        "registered",
        "dob",
        "phone",
        "cell",
        "id",
        "picture",
        "nat"
})
public class User {

    @JsonProperty("gender")
    private String gender;
    @JsonProperty("name")
    private Name name;
    @JsonProperty("location")
    private Location location;
    @JsonProperty("email")
    private String email;
    @JsonProperty("login")
    private Login login;
    @JsonProperty("registered")
    private Integer registered;
    @JsonProperty("dob")
    private Integer dob;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("cell")
    private String cell;
    @JsonProperty("id")
    private Id id;
    @JsonProperty("picture")
    private Picture picture;
    @JsonProperty("nat")
    private String nat;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The gender
     */
    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The name
     */
    @JsonProperty("name")
    public Name getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(Name name) {
        this.name = name;
    }

    /**
     * @return The location
     */
    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The login
     */
    @JsonProperty("login")
    public Login getLogin() {
        return login;
    }

    /**
     * @param login The login
     */
    @JsonProperty("login")
    public void setLogin(Login login) {
        this.login = login;
    }

    /**
     * @return The registered
     */
    @JsonProperty("registered")
    public Integer getRegistered() {
        return registered;
    }

    /**
     * @param registered The registered
     */
    @JsonProperty("registered")
    public void setRegistered(Integer registered) {
        this.registered = registered;
    }

    /**
     * @return The dob
     */
    @JsonProperty("dob")
    public Integer getDob() {
        return dob;
    }

    /**
     * @param dob The dob
     */
    @JsonProperty("dob")
    public void setDob(Integer dob) {
        this.dob = dob;
    }

    /**
     * @return The phone
     */
    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The cell
     */
    @JsonProperty("cell")
    public String getCell() {
        return cell;
    }

    /**
     * @param cell The cell
     */
    @JsonProperty("cell")
    public void setCell(String cell) {
        this.cell = cell;
    }

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Id getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Id id) {
        this.id = id;
    }

    /**
     * @return The picture
     */
    @JsonProperty("picture")
    public Picture getPicture() {
        return picture;
    }

    /**
     * @param picture The picture
     */
    @JsonProperty("picture")
    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    /**
     * @return The nat
     */
    @JsonProperty("nat")
    public String getNat() {
        return nat;
    }

    /**
     * @param nat The nat
     */
    @JsonProperty("nat")
    public void setNat(String nat) {
        this.nat = nat;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
