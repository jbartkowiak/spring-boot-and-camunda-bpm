package de.bartkowiak.demo.domain.user;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "username",
        "password",
        "salt",
        "md5",
        "sha1",
        "sha256"
})
public class Login {

    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;
    @JsonProperty("salt")
    private String salt;
    @JsonProperty("md5")
    private String md5;
    @JsonProperty("sha1")
    private String sha1;
    @JsonProperty("sha256")
    private String sha256;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The username
     */
    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The password
     */
    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    /**
     * @param password The password
     */
    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return The salt
     */
    @JsonProperty("salt")
    public String getSalt() {
        return salt;
    }

    /**
     * @param salt The salt
     */
    @JsonProperty("salt")
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * @return The md5
     */
    @JsonProperty("md5")
    public String getMd5() {
        return md5;
    }

    /**
     * @param md5 The md5
     */
    @JsonProperty("md5")
    public void setMd5(String md5) {
        this.md5 = md5;
    }

    /**
     * @return The sha1
     */
    @JsonProperty("sha1")
    public String getSha1() {
        return sha1;
    }

    /**
     * @param sha1 The sha1
     */
    @JsonProperty("sha1")
    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }

    /**
     * @return The sha256
     */
    @JsonProperty("sha256")
    public String getSha256() {
        return sha256;
    }

    /**
     * @param sha256 The sha256
     */
    @JsonProperty("sha256")
    public void setSha256(String sha256) {
        this.sha256 = sha256;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}