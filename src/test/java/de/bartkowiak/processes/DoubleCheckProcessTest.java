package de.bartkowiak.processes;

import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.spring.boot.starter.test.helper.AbstractProcessEngineRuleTest;
import org.junit.Before;
import org.junit.Test;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.assertThat;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;

@Deployment(resources = "bpmn/doubleCheck.bpmn")
public class DoubleCheckProcessTest extends AbstractProcessEngineRuleTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testWithApprovingAndClearance_ExpectsNoErrors() throws Exception {
        final ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("DoubleCheck");

        assertThat(processInstance)
                .isWaitingAt("UserTask_1goxse8")
                .task()
                .isNotAssigned();

        claim(task(), "approverOne");
        complete(task(), withVariables("approval", true));

        assertThat(processInstance)
                .isWaitingAt("UserTask_0pqobkz")
                .task()
                .isNotAssigned();

        claim(task(), "approverTwo");
        complete(task(), withVariables("clearance", true));

        assertThat(processInstance).isEnded();
    }
}
