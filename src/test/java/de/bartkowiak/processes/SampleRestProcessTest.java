package de.bartkowiak.processes;

import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.spring.boot.starter.test.helper.AbstractProcessEngineRuleTest;
import org.junit.Before;
import org.junit.Test;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;
import static org.camunda.bpm.extension.mockito.DelegateExpressions.registerJavaDelegateMock;
import static org.camunda.bpm.extension.mockito.DelegateExpressions.verifyJavaDelegateMock;
import static org.mockito.Mockito.times;

@Deployment(resources = "bpmn/sampleRest.bpmn")
public class SampleRestProcessTest extends AbstractProcessEngineRuleTest {

    private static final String NAME_DELEGATE = "nameDelegate";
    private static final String HELLO_DELEGATE = "helloDelegate";
    
    @Before
    public void setUp() throws Exception {
        // Or use autoMock("bpmn/sampleRest.bpmn");
        registerJavaDelegateMock(NAME_DELEGATE);
        registerJavaDelegateMock(HELLO_DELEGATE);
    }

    @Test
    public void testDoubleCheck_WithValidData_ExpectsDoubleCheckedEnd() throws Exception {
        final ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("SampleRest");

        assertThat(processInstance).isWaitingAt("ServiceTask_1");
        execute(job());
        verifyJavaDelegateMock(NAME_DELEGATE).executed(times(1));

        assertThat(processInstance).isWaitingAt("ServiceTask_2");
        execute(job());
        verifyJavaDelegateMock(HELLO_DELEGATE).executed(times(1));

        assertThat(processInstance).isEnded();
    }
}
