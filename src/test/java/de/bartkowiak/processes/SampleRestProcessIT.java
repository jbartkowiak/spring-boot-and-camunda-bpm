package de.bartkowiak.processes;

import de.bartkowiak.demo.DemoApplication;
import de.bartkowiak.demo.services.internal.hello.HelloDelegate;
import de.bartkowiak.demo.services.internal.hello.HelloService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
public class SampleRestProcessIT {

    @InjectMocks
    private HelloDelegate helloDelegate;
    @Mock
    private HelloService helloService;
    @Autowired
    private RuntimeService runtimeService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void simpleProcessTest() {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("SampleRest");

        assertThat(processInstance)
                .job("ServiceTask_1")
                .isNotNull();
        execute(job("ServiceTask_1", processInstance));

        assertThat(processInstance)
                .job("ServiceTask_2")
                .isNotNull();
        execute(job("ServiceTask_2", processInstance));
    }
}
